package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<E> extends Node<E> implements IQueue<E>, Iterable<E> 
{
	/**
	 * The first node in the queue.
	 */
	private Node<E> first;    

	/**
	 * The last node in the queue.
	 */
	private Node<E> last;     

	/**
	 * Number of elements n in the queue.
	 */
	private int n;

	/**
	 * Initializes an empty queue.
	 */
	public Queue() 
	{
		first = null;
		last  = null;
		n = 0;
	}

	/**
     * Adds the item to this queue.
     * @param  item the item to add
     */
	@Override
	public void enqueue(E item) 
	{
		Node<E> oldlast = last;
		last = new Node<E>();
		item = last.getItem();
		oldlast.changeNext(null);
		if (isEmpty())
		{
			first = last;
		}
		else        
		{
			oldlast.changeNext(last);
		}
		n++;
	}

	
	/**
     * Removes and returns the item on this queue that was least recently added.
     * @return the item on this queue that was least recently added
     * @throws NoSuchElementException if this queue is empty
     */
	@Override
	public E dequeue() 
	{
		if (isEmpty())
		{
			throw new NoSuchElementException("Queue underflow");
		}
		E item = first.getItem();
		first = first.getNext();
		n--;
		if (isEmpty())
		{
			last = null;   // to avoid loitering
		}
		return item;
	}

	/**
	 * Returns true if this queue is empty.
	 * @return true if this queue is empty; false otherwise
	 */
	@Override
	public boolean isEmpty() 
	{
		boolean isEmpty = false;
		if(first == null)
		{
			isEmpty = true;
		}
		return isEmpty;
	}

	/**
	 * Returns the number of items in this stack.
	 * @return the number of items in this stack
	 */
	public int size() 
	{
		return n;
	}


	@Override
	public Iterator<E> iterator() 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
