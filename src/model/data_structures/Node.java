package model.data_structures;

public class Node<E> 
{
	private E item;
	private Node<E> next;
	
	public E getItem()
	{
		return item;
	}
	
	public Node<E> getNext()
	{
		return next;
	}
	
	public void changeNext(Node<E> pNext)
	{
		next = pNext;
	}
}
